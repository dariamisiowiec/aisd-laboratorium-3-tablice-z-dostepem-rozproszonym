package tabliceRozproszone;

import java.io.File;

public class PodwojneRozpraszanieSecondFunction {

    private int tableSize = 20000;
    private String[] words = new String[tableSize];
    private int counter = 0;

    public int getCounter() {
        return counter;
    }

    private int calculateASCIIcodeSum(String word) {
        int sum = 0;
        for (int i = 0; i < word.length(); i++) {
            sum += (int)word.charAt(i);
        }
        return sum;
    }

    public int secondHashFunction (String word) {
        int hash = 5381;
        for (int i = 0; i < word.length(); i++) {
            hash = (hash*33 + (int)word.charAt(i))%tableSize;
        }
        return hash;
    }

    public int thirdHashFunction (String word) {
        return (calculateASCIIcodeSum(word))%19997 + 1;
    }

    public void podwojneRozpraszanieDlaDrugiejFunkcji (String word) {
        int i = 1;
        int hash;
        counter = 0;
        while (i<tableSize) {
            counter++;
            hash = (secondHashFunction(word) + i*thirdHashFunction(word))%tableSize;
            if (words[hash] == "null") {
                words[hash] = word;
                break;
            }
            else
                i+=1;
        }
        if (i == tableSize)
            System.out.println("No space available");
    }

    public int findKey_podwojneRozpraszanie_secondFunction (String word) {
        int i = 0 ;
        int j;
        counter = 0;
        do {
            counter++;
            j = (secondHashFunction(word) + i*thirdHashFunction(word))%tableSize;
            if (words[j].equals(word)) {
                return j;
            }
            else {
                i = i+1;
            }
        } while (i < tableSize && words[j] != "null");
        System.out.println("Record not found");
        return -1;
    }
}

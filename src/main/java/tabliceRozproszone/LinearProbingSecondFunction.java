package tabliceRozproszone;

import java.io.File;

public class LinearProbingSecondFunction {

    private int tableSize = 20000;
    private String[] words = new String[tableSize];
    private int counter = 0;

    public int getCounter() {
        return counter;
    }

    public int secondHashFunctionForLinearProbing (String word, int i) {
        int hash = 5381;
        for (int j = 0; j < word.length(); j++) {
            hash = (hash*33 + (int)word.charAt(j) + i)%tableSize;
        }
        return hash;
    }

    public void linearProbingForSecondFunction (String word) {
        int i = 0;
        int hash = 0;
        counter = 0;
        while (i<tableSize) {
            counter++;
            hash = secondHashFunctionForLinearProbing(word, i);
            if (words[hash] == null) {
                words[hash] = word;
                break;
            }
            else
                i+=1;
        }
   //     if (i == tableSize)
   //         System.out.println("No space available");
    }

    public int findKey_linearProbing_secondFunction (String word) {
        int i = 0 ;
        int j;
        counter = 0;
        do {
            counter++;
            j = secondHashFunctionForLinearProbing(word, i);
            if (words[j].equals(word)) {
                return j;
            }
            else {
                i = i+1;
            }
        } while (i < tableSize && words[j] != "null");
        System.out.println("Record not found");
        return -1;
    }
}

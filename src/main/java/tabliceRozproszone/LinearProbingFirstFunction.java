package tabliceRozproszone;


import java.util.Arrays;

public class LinearProbingFirstFunction {
    private int tableSize = 20000;
    private String[] words = new String[tableSize];
    private int counter = 0;

    public int getCounter() {
        return counter;
    }

    public LinearProbingFirstFunction() {
        tableSize = 20000;
        words = new String[20000];
    }

    public String getWords() {
        return Arrays.toString(words);
    }

    public int calculateASCIIcodeSum(String word) {
        int sum = 0;
        for (int i = 0; i < word.length(); i++) {
            sum += (int)word.charAt(i);
        }
        return sum;
    }

    private int firstHashFunctionForLinearProbing (String word, int i) {
        return (calculateASCIIcodeSum(word) + i)%tableSize;
    }

    public void linearProbingForFirstFunction (String word) {
        int i = 0;
        int hash = 0;
        counter = 0;
        while (i<tableSize) {
            counter++;
            hash = firstHashFunctionForLinearProbing(word, i);
            if (words[hash]== null) {
                words[hash] = word;
                break;
            }
            else
                i+=1;
        }
 //       if (i == tableSize)
//            System.out.println("No space available");
    }

    public int findKey_linearProbing_firstFunction (String word) {
        int i = 0 ;
        int j;
        counter = 0;

        do {
            counter++;
            j = firstHashFunctionForLinearProbing(word, i);
            if (words[j] == null) {
   //             System.out.println("Record not found");
                return -1;
            }
            else if (words[j].equals(word)) {
                return j;
            }
            else {
                i = i+1;
            }
        } while (i < tableSize && words[j]!= null);
//        System.out.println("Record not found");
        return -1;
    }
}

package tabliceRozproszone;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        LinearProbingFirstFunction linearProbingFirstFunction = new LinearProbingFirstFunction();
        float mean = 0;

        File file = new File ("slowa.txt");
        try {
            Scanner scanner = new Scanner(file);
            for (int i = 0; i < 5000; i++) {
                String word = scanner.next();
                linearProbingFirstFunction.linearProbingForFirstFunction(word);
            }
            File file2 = new File ("slowa.txt");
            Scanner scanner2 = new Scanner(file2);
            for (int i = 0; i < 1000; i++) {
                String word = scanner2.next();
                linearProbingFirstFunction.findKey_linearProbing_firstFunction(word);
                mean += linearProbingFirstFunction.getCounter();
            }
            mean = mean/1000;
            System.out.println("Linear Probing - first function - 5000 elements inside table - finding element mean: "
                                + mean);

            mean = 0;
            for (int i = 0; i < 1000; i++) {
                String word = scanner.next();
                linearProbingFirstFunction.linearProbingForFirstFunction(word);
                mean = mean + linearProbingFirstFunction.getCounter();
            }
            mean = mean/1000;
            System.out.println("Linear Probing - first function - 5000 elements inside table - adding element mean: "
                    + mean);


            for (int i = 0; i < 1000; i++) {
                String word = scanner.next();
                linearProbingFirstFunction.linearProbingForFirstFunction(word);
            }

            mean = 0;
            for (int i = 0; i < 1000; i++) {
                String word = scanner2.next();
                linearProbingFirstFunction.findKey_linearProbing_firstFunction(word);
                mean += linearProbingFirstFunction.getCounter();
            }
            mean = mean/1000;
            System.out.println("Linear Probing - first function - 7000 elements inside table - finding element mean: "
                    + mean);

            mean = 0;
            for (int i = 0; i < 1000; i++) {
                String word = scanner.next();
                linearProbingFirstFunction.linearProbingForFirstFunction(word);
                mean = mean + linearProbingFirstFunction.getCounter();
            }
            mean = mean/1000;
            System.out.println("Linear Probing - first function - 7000 elements inside table - adding element mean: "
                    + mean);

            for (int i = 0; i < 1000; i++) {
                String word = scanner.next();
                linearProbingFirstFunction.linearProbingForFirstFunction(word);
            }

            mean = 0;
            for (int i = 0; i < 1000; i++) {
                String word = scanner2.next();
                linearProbingFirstFunction.findKey_linearProbing_firstFunction(word);
                mean += linearProbingFirstFunction.getCounter();
            }
            mean = mean/1000;
            System.out.println("Linear Probing - first function - 9000 elements inside table - finding element mean: "
                    + mean);

            mean = 0;
            for (int i = 0; i < 1000; i++) {
                String word = scanner.next();
                linearProbingFirstFunction.linearProbingForFirstFunction(word);
                mean = mean + linearProbingFirstFunction.getCounter();
            }
            mean = mean/1000;
            System.out.println("Linear Probing - first function - 9000 elements inside table - adding element mean: "
                    + mean);

        } catch (FileNotFoundException fnfe) {
            fnfe.printStackTrace();
        }


        LinearProbingSecondFunction linearProbingSecondFunction = new LinearProbingSecondFunction();
        mean = 0;

       // File file = new File ("slowa.txt");
        try {
            Scanner scanner = new Scanner(file);
            for (int i = 0; i < 5000; i++) {
                String word = scanner.next();
                linearProbingSecondFunction.linearProbingForSecondFunction(word);
            }
            File file2 = new File ("slowa.txt");
            Scanner scanner2 = new Scanner(file2);
            for (int i = 0; i < 1000; i++) {
                String word = scanner2.next();
                linearProbingSecondFunction.findKey_linearProbing_secondFunction(word);
                mean += linearProbingSecondFunction.getCounter();
            }
            mean = mean/1000;
            System.out.println("Linear Probing - second function - 5000 elements inside table - finding element mean: "
                    + mean);

            mean = 0;
            for (int i = 0; i < 1000; i++) {
                String word = scanner.next();
                linearProbingSecondFunction.linearProbingForSecondFunction(word);
                mean = mean + linearProbingSecondFunction.getCounter();
            }
            mean = mean/1000;
            System.out.println("Linear Probing - second function - 5000 elements inside table - adding element mean: "
                    + mean);


            for (int i = 0; i < 1000; i++) {
                String word = scanner.next();
                linearProbingSecondFunction.linearProbingForSecondFunction(word);
            }

            mean = 0;
            for (int i = 0; i < 1000; i++) {
                String word = scanner2.next();
                linearProbingSecondFunction.findKey_linearProbing_secondFunction(word);
                mean += linearProbingSecondFunction.getCounter();
            }
            mean = mean/1000;
            System.out.println("Linear Probing - second function - 7000 elements inside table - finding element mean: "
                    + mean);

            mean = 0;
            for (int i = 0; i < 1000; i++) {
                String word = scanner.next();
                linearProbingSecondFunction.linearProbingForSecondFunction(word);
                mean = mean + linearProbingSecondFunction.getCounter();
            }
            mean = mean/1000;
            System.out.println("Linear Probing - second function - 7000 elements inside table - adding element mean: "
                    + mean);

            for (int i = 0; i < 1000; i++) {
                String word = scanner.next();
                linearProbingSecondFunction.linearProbingForSecondFunction(word);
            }

            mean = 0;
            for (int i = 0; i < 1000; i++) {
                String word = scanner2.next();
                linearProbingSecondFunction.findKey_linearProbing_secondFunction(word);
                mean += linearProbingSecondFunction.getCounter();
            }
            mean = mean/1000;
            System.out.println("Linear Probing - second function - 9000 elements inside table - finding element mean: "
                    + mean);

            mean = 0;
            for (int i = 0; i < 1000; i++) {
                String word = scanner.next();
                linearProbingSecondFunction.linearProbingForSecondFunction(word);
                mean = mean + linearProbingSecondFunction.getCounter();
            }
            mean = mean/1000;
            System.out.println("Linear Probing - second function - 9000 elements inside table - adding element mean: "
                    + mean);

        } catch (FileNotFoundException fnfe) {
            fnfe.printStackTrace();
        }

    LinearProbingSecondFunction linearProbingSecondFunction1 = new LinearProbingSecondFunction();
        System.out.println(linearProbingSecondFunction.secondHashFunctionForLinearProbing("a", 0));







    }
}

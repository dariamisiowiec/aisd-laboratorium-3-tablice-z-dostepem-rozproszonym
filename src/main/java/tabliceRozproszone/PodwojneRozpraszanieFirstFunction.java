package tabliceRozproszone;

import java.io.File;

public class PodwojneRozpraszanieFirstFunction {

    private int tableSize = 20000;
    private String[] words = new String[tableSize];
    private int counter = 0;

    public int getCounter() {
        return counter;
    }

    private int calculateASCIIcodeSum(String word) {
        int sum = 0;
        for (int i = 0; i < word.length(); i++) {
            sum += (int)word.charAt(i);
        }
        return sum;
    }

    public int firstHashFunction (String word) {
        return calculateASCIIcodeSum(word)%tableSize;
    }

    public int thirdHashFunction (String word) {
        return (calculateASCIIcodeSum(word))%19997 + 1;
    }

    public void podwojneRozpraszanieDlaPierwszejFunkcji (String word) {
        int i = 1;
        int hash;
        counter = 0;
        while (i<tableSize) {
            counter++;
            hash = (firstHashFunction(word) + i*thirdHashFunction(word))%tableSize;
            if (words[hash] == "null") {
                words[hash] = word;
                break;
            }
            else
                i+=1;
        }
        if (i == tableSize)
            System.out.println("No space available");
    }

    public int findKey_dowojneRozpraszanie_firstFunction (String word) {
        int i = 0 ;
        int j;
        counter = 0;
        do {
            counter ++;
            j = (firstHashFunction(word) + i*thirdHashFunction(word))%tableSize;
            if (words[j].equals(word)) {
                return j;
            }
            else {
                i = i+1;
            }
        } while (i < tableSize && words[j] != "null");
        System.out.println("Record not found");
        return -1;
    }
}
